/**
 * Main class of the package game to administer game logic.
 * @author Joris Gutjahr
 * @version 0.1.0
 *
 */
package game;

import java.util.ArrayList;

public class Game extends Thread {

  public Tile[] tiles;
  public User winner = null;
  User[] players;
  int xDimension;
  int yDimension;

  /**
   * Create a new game.
   * @param xDimension Tiles in X Direction.
   * @param yDimension Tiles in Y Direction
   * @param humanPlayers Number of human players in the game. All other tiles are given to AI Players.
   * @param callbacks UI callbacks. view function is called after a round.
   */
  public Game(int xDimension, int yDimension, String humanPlayers[]) {

    this.xDimension = xDimension;
    this.yDimension = yDimension;

    this.tiles = new Tile[xDimension * yDimension];
    this.players = new User[xDimension * yDimension];

    // Create users and tiles.
    int index = 0;
    for(int x = 0; x < xDimension; x++) {
      for(int y = 0; y < yDimension; y++) {
        String name = humanPlayers.length > index ? "*" : "";
        this.tiles[index] = new Tile(x, y, this);
        if(humanPlayers.length > index) {
          name = humanPlayers[index];
        } else {
          name = "Computer " + String.valueOf(index - humanPlayers.length);
        }
        User user = new User( humanPlayers.length > index
                            , name
                            , this
                            );
        this.players[index] = user;
        this.tiles[index].owner = user;
        index++;
      }
    }
  }

  /**
   * Initialize the round.
   * Let every AI user register their moves.
   */
  public void beforeRound() {
    if(!allTilesAreReady(tiles)) {
      for (Tile tile: tiles) {
        if (!tile.owner.isHuman) {
          tile.owner.init(tile);
        }
      }

      for(User user: players) {
        if(!user.isHuman) {
          user.makeReady();
        }
      }
    }

    while(!allTilesAreReady(tiles)) {
      try {
        Thread.sleep(100);
      } catch(InterruptedException e) {
        continue;
      }
    }
  }

  /**
   * One of the conditions to start a round is,
   * that all tiles are ready i.e. have their orders.
   */
  public boolean allTilesAreReady(Tile[] tiles) {
    for(Tile tile : tiles) {
      if (!tile.isReady()) {
        return false;
      }
    }
    return true;
  }

  /**
   * Check the victory condition and reset the users actions.
   */
  public void afterRound() {
    User possibleWinner = tiles[0].owner;

    for(User player : players) {
      player.reset();
    }

    for(Tile tile : tiles) {
      if(tile.owner != possibleWinner) {
        possibleWinner = null;
        break;
      }
    }

    this.winner = possibleWinner;

    if(this.winner == null) {
      for(Tile tile : tiles) {
        tile.reset();
      }
    }
  }


  /**
   * Called once every human player has made their moves.
   * A round:
   * 1. Check if the victory condition is met already. If so, do not continue.
   * 2. Tiles are updated (attacks are evaluated)
   */
  public void round() {
    // You cannot executed another round, after the victory condition was met.
    if(hasWinner()) return;

    for(Tile tile : tiles) {
      if(tile.state == null) tile.state = Tile.Actions.DEFEND;

      if(tile.state == Tile.Actions.ATTACK && tile.getAttackedTile() == null) {
        tile.state = Tile.Actions.DEFEND;
      }

      switch(tile.state) {
        case ATTACK:
          Tile attacked = tile.getAttackedTile();
          String attackString;

          if(attacked != null) {
            attackString = attacked.toString();
          } else {
            attackString = "null";
          }

          System.out.println(tile.toString() + " attacks " + attacked.toString());

          // Skip invalid attack attempts
          if(attacked == null || !tile.isNeighbour(attacked)) {
            break;
          }

          if(attacked.state == Tile.Actions.ATTACK) {
            attacked.owner = tile.owner;
            attacked.state = Tile.Actions.CONQUERED;
            System.out.println(tile.owner.toString() + " conquered " + attacked.toString());
          } else if(attacked.attackers.size() > 0) {
            if(allAttackersAreEqual(attacked.attackers) && attacked.attackers.get(0) == tile.owner) {
              attacked.owner = tile.owner;
              attacked.state = Tile.Actions.CONQUERED;
              System.out.println(tile.owner.toString() + " conquered " + attacked.toString());
            } else {
              attacked.attackers.add(tile.owner);
            }
          }
          break;
        case DEFEND:
          break;
        case CONQUERED:
          break;
      }
    }
  }

  /**
   * Wait for the players to be ready, then play, then clean up and repeat
   * until a winner has bee found
   */
  @Override
  public void run() {
    do {
      System.out.println("Playing round");
      beforeRound();
      round();
      afterRound();
    } while(!hasWinner());
  }

  /**
   * @return boolean true if all the attackers are equal.
   */
  public boolean allAttackersAreEqual(ArrayList<User> attackers) {
    if(attackers.size() == 0) return true;
    User attacker = attackers.get(0);
    for(int i = 1; i < attackers.size(); i++) {
      if(attackers.get(i) != attacker) return false;
    }
    return true;
  }

  /**
   * @return boolean true if a user has won.
   */
  public boolean hasWinner() {
    return this.winner != null;
  }

  /**
   * @return User the winner or null if there is no winner.
   */
  public User getWinner() {
    return this.winner;
  }

  /**
   * @return ArrayList<Tile> of all the Tiles currently owned by the user.
   */
  public ArrayList<Tile> ownTiles(User user) {
    ArrayList<Tile> ownTiles = new ArrayList<Tile>();
    for(Tile tile: tiles) {
      if(tile.owner == user) {
        ownTiles.add(tile);
      }
    }
    return ownTiles;
  }
}
