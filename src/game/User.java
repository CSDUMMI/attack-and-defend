/**
 * A User either AI or human.
 */
package game;

import game.Tile.Actions;
import java.util.Random;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.HashMap;

public class User {

  boolean isHuman;
  boolean isReady = false;
  Tile selected;
  Game game;
  String name;

  public User(boolean isHuman, String name, Game game) {
    this.isHuman = isHuman;
    this.game = game;
    this.name = name;
  }


  public String getName() {
    return name;
  }

  public String toString() {
    return this.getName();
  }

  /**
   * Callback to select another tile to attack.
   * This only works if the user is human.
   * @param tile the tile to attack or if it is the selected tile, to defend.
   */
  public void attack(Tile tile) {
    if(isHuman) {
      if(selected == null) {
        return;
      } else if(tile != selected) {
        selected.state = Actions.ATTACK;
        selected.attacking = tile;
      } else {
        selected.state = Actions.DEFEND;
        selected.attacking = null;
      }
    }

  }

  /**
   * Select another tile. Only for human users.
   * @param tile tile to select
   * @return boolean true if the tile has been selected and the user is a human.
   */
  public boolean select(Tile tile) {
    if(isHuman) {
      if(tile == selected) {
        selected = null;
        return true;
      } else if(tile.owner == this) {
        selected = tile;
        return true;
      } else {
        return false;
      }
    }  else {
      return false;
    }
  }

  public boolean isSelected(Tile tile) {
    return tile == selected;
  }

  public Tile getSelected() {
    return this.selected;
  }

  /**
   * @return boolean true if the tile is being attacked by the selected tile.
   */
  public boolean attacks(Tile tile) {
    return this.selected != null && this.selected.isAttacking(tile);
  }

  /**
   * @return boolean true if the user is ready to begin the new round.
   */
  public boolean ready() {
    return this.game.ownTiles(this).isEmpty() || isReady;
  }

  /**
   * Set isReady to true
   * and set all owned tiles,
   * where state = null to Actions.DEFEND
   */
  public void makeReady() {
    isReady = true;
    for(Tile tile : this.game.ownTiles(this)) {
      if(tile.state == null) {
        tile.state = Actions.DEFEND;
      }
      if(tile.state != Actions.ATTACK) {
        tile.attacking = null;
      }
    }
  }

  public void reset() {
    isReady = false;
  }

  // AI Functions
  /**
   * Initialize a Tile for an AI Player
   * @param tile the Tile to initiate.
   */
  public void init(Tile tile) {
    if(tile.owner != this || isHuman || tile.isReady() ) return;

    Tile[] neighbours = tile.getNeighbours();
    Random random = new Random();

    HashMap<User, ArrayList<Tile>> proportions = new HashMap<User, ArrayList<Tile>>();

    for(Tile neighbour : neighbours) {
      ArrayList<Tile> owned = proportions.get(neighbour.owner);
      if(owned == null) {
        owned = new ArrayList<Tile>();
        owned.add(neighbour);
        proportions.put(neighbour.owner, owned);
      } else {
        owned.add(neighbour);
        proportions.replace(neighbour.owner, owned);
      }
    }

    int[] sizes = new int[proportions.size()];
    double[] parts = new double[proportions.size()];
    int i = 0;
    for (User user : proportions.keySet()) {
      sizes[i] = proportions.get(user).size();
      parts[i] = sizes[i]/neighbours.length;
      i++;
    }
    Arrays.sort(parts);

    double largestNeighbour = parts[parts.length -1];
    if(largestNeighbour > 2/3) {
      tile.state = Actions.ATTACK;
      tile.attacking = selectTarget(neighbours);

      if(tile.attacking == null) {
        tile.state = Actions.DEFEND;
      }
    } else if(largestNeighbour > 1/2) {
      tile.state = Actions.DEFEND;
    } else if(largestNeighbour < 1/2) {
      double decision = random.nextDouble();
      if(decision > 0.1) {
        tile.state = Actions.ATTACK;
        tile.attacking = selectTarget(neighbours);
        if(tile.attacking == null) tile.state = Actions.DEFEND;
      } else {
        tile.state = Actions.DEFEND;
      }
    } else {
      double decision = random.nextDouble();
      if(decision > 0.2) {
        tile.state = Actions.ATTACK;
        tile.attacking = selectTarget(neighbours);
        if(tile.attacking == null) tile.state = Actions.DEFEND;
      } else {
        tile.state = Actions.DEFEND;
      }
    }
  }

  /**
   * @param tiles Tiles to select a target to attack from.
   * @return Tile to attack or null if no one should be attacked.
   */
  public Tile selectTarget(Tile[] tiles) {
    int ownedNeighbours = 1;
    for(int i = 0; i < tiles.length; i++) {
      if(tiles[i].owner == this) {
        tiles[i] = null;
        ownedNeighbours++;
      }
    }

    if(ownedNeighbours == tiles.length) return null;

    Random random = new Random();
    int index = random.nextInt(tiles.length);
    int initial = index;

    while(tiles[index] == null) {
      index++;
      if(index >= tiles.length) index = 0;
      if(index == initial) return null;
    }

    return tiles[index];
  }
}
