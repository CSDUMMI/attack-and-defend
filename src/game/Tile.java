/**
 * Class for a single Tile.
 * @author Joris Gutjahr
 * @version 0.1.0
 * @since 23. Oct 2020
 */
package game;

import java.util.ArrayList;

public class Tile {
  int x;
  int y;
  public Game game;
  public User owner;

  public enum Actions {
    ATTACK,
    DEFEND,
    CONQUERED
  }

  protected Actions state = Actions.DEFEND;
  protected Tile attacking;

  public ArrayList<game.User> attackers;

  /**
   * @param x X Coordinate of the Tile
   * @param y Y Coordinate of the Tile
   * @param game Game that this Tile belongs to.
   */
  public Tile(int x, int y, Game game) {
    this.x = x;
    this.y = y;
    this.game = game;
    this.attackers = new ArrayList<User>();
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  public String toString() {
    return "(" + x + "|" + y + ")";
  }

  public Tile getAttackedTile() {
    return attacking;
  }

  public Actions getState() {
    return state;
  }

  /**
   * @return boolean true if the Tile provided is being attacked.
   */
  public boolean isAttacking(Tile tile) {
    if(attacking != null) {
      return tile == attacking;
    } else {
      return false;
    }
  }

  public Tile[] getNeighbours() {
    ArrayList<Tile> neighbours = new ArrayList<Tile>();
    for(Tile tile : game.tiles) {
      if(isNeighbour(tile)) {
        neighbours.add(tile);
      }
    }
    Tile[] asArray = new Tile[neighbours.size()];
    neighbours.toArray(asArray);
    return asArray;
  }

  /**
   * @param possibleNeighbour the Tile, that may or may not be a neighbour.
   * @return boolean true if the possibleNeighbour is a neighbour.
   */
  public boolean isNeighbour(Tile possibleNeighbour) {
    boolean xNeighbour = absolute(possibleNeighbour.getX() - x) <= 1;
    boolean yNeighbour = absolute(possibleNeighbour.getY() - y) <= 1;
    return xNeighbour && yNeighbour && possibleNeighbour.game == game;
  }

  public int absolute(int x) {
    if(x < 0) {
      return x * -1;
    } else {
      return x;
    }
  }

  /**
   * @return boolean true if the state and attacking is set and user is ready. (Only if the user is a human).
   */
  public boolean isReady() {
    return this.state != null && (this.state == Actions.ATTACK || this.attacking == null) && this.owner.ready();
  }

  /**
   * Resets the state and attacking.
   */
  public void reset() {
    this.state = null;
    this.attacking = null;
    this.attackers.clear();
  }
}
