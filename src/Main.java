import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.input.MouseEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.text.Text;
import javafx.scene.Group;
import javafx.scene.control.TextField;
import javafx.animation.AnimationTimer;
import javafx.stage.Stage;
import java.util.Arrays;
import game.*;
import IO.TilePane;

/**
 * Implements the interface of the game.
 * @author Joris Gutjahr
 */
public class Main extends Application {
    Stage primaryStage;
    Game game;
    User user;
    double WIDTH = 500;
    double HEIGHT = 500;
    ScrollPane tutorial;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        primaryStage.setTitle("Attack & Defend");
        primaryStage.setScene(this.menu());
        primaryStage.show();

        tutorial = new ScrollPane();
        String text = "Rules\n"
                      + "1. Your tiles are green\n"
                      + "2. Each round a tile can either attack or defend\n"
                      + "3. The violet tile is the one you are currently selecting\n"
                      + "4. The goal is it, to conquere all the tiles\n"
                      + "5. To conquere a tile, you can either attack it,\nwhile it attacks"
                      + "some other tile or\nyou can attack from two sides at once.\n"
                      + "6. To attack a tile, first select one of your own (the attacker)\n"
                      + "and then click on the one you want to attck.\n"
                      + "7. The tile you are currently attacking is red.\n"
                      + "8. You can only attack your neighbours.\n"
                      + "9. Any tile, that does not attack, defends itself\n"
                      + "10. You may want to limit your own attack surface\nand increase your attacking oppertunities\n";
        tutorial.setContent(new Text(text));
    }

    // VIEW Functions
    /**
     * Create the starting menu
    */
    public Scene menu() {

      Button newGame = new Button();
      newGame.setText("Create a Game");
      newGame.setOnAction(new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
          // Create dialog for creating the game.
          primaryStage.setTitle("Create a game");

          double min = 2;
          double max = 16;
          Slider widthSlider = new Slider(min, max, min+1);
          Slider heightSlider = new Slider(min, max, min+1);

          widthSlider.setBlockIncrement(1);
          heightSlider.setBlockIncrement(1);

          widthSlider.setShowTickMarks(true);
          heightSlider.setShowTickMarks(true);

          widthSlider.setShowTickLabels(true);
          heightSlider.setShowTickLabels(true);

          Label widthLabel = new Label("Width");
          Label heightLabel = new Label("Height");

          widthLabel.setLabelFor(widthSlider);
          heightLabel.setLabelFor(widthSlider);

          TextField nameField = new TextField(System.getProperty("user.name"));

          Button submit = new Button();
          submit.setText("Create Game");
          submit.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent event) {
              int width = (int) widthSlider.getValue();
              int height = (int) heightSlider.getValue();
              String[] names = new String[1];
              names[0] = nameField.getCharacters().toString();
              newGame(width, height, names);
            }
          });


          VBox root = new VBox( 8
                              , new Label("Create a new game")
                              , widthLabel
                              , widthSlider
                              , heightLabel
                              , heightSlider
                              , nameField
                              , submit
                              );

          Scene scene = new Scene(root, primaryStage.getWidth(), primaryStage.getHeight());
          primaryStage.setScene(scene);
          primaryStage.show();
        }
      });

      StackPane root = new StackPane();
      root.getChildren().add(newGame);
      Scene scene = new Scene(root, WIDTH, HEIGHT);
      return scene;
    }

    /**
     * @param width number of columns
     * @param height number of rows
     * @param players number of human players.
     */
    public void newGame(int width, int height, String players[]) {
      game = new Game(width, height, players);
      game.start();
      user = game.tiles[0].owner;

      AnimationTimer timer = new AnimationTimer() {
        public void handle(long now) {
          if(game.hasWinner()) {
            primaryStage.setTitle("Winner is: " + game.getWinner().getName());
            Button exit = new Button("Exit");
            Label winnerIs = new Label("Winner is: " + game.getWinner().getName());
            exit.setOnMousePressed(new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent event) {
                System.exit(0);
              }
            });
            VBox root = new VBox(8);
            root.setPrefWidth(WIDTH);
            root.setPrefHeight(HEIGHT);
            root.getChildren().addAll(winnerIs,exit);
            primaryStage.setScene(new Scene(root));
            primaryStage.show();
            return;
          }
          VBox boardSide = new VBox();
          Group board = new Group();
          double boardWidth = WIDTH;
          double boardHeight = 0.8*HEIGHT;
          double tileWidth = boardWidth/width;
          double tileHeight = boardHeight/height;

          for(Tile tile: game.tiles) {
            EventHandler<MouseEvent> handler = new EventHandler<MouseEvent>() {
              @Override
              public void handle(MouseEvent e) {
                if(user.isSelected(tile) || user != tile.owner) {
                  if(user.getSelected() != null && user.getSelected().isNeighbour(tile)) {
                    user.attack(tile);
                  }
                } else if(tile.owner == user) {
                  user.select(tile);
                }
              }
            };

            StackPane pane = TilePane.tilePane( tile
                                              , tileWidth
                                              , tileHeight
                                              , user
                                              , handler
                                              );

            board.getChildren().add(pane);
          }

          Button ready = new Button("Ready");
          ready.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
              user.makeReady();
            }
          });
          boardSide.getChildren().add(board);
          boardSide.getChildren().add(ready);

          HBox root = new HBox(4);
          root.getChildren().add(boardSide);
          root.getChildren().add(tutorial);
          primaryStage.setScene(new Scene(root, boardWidth + 500, boardHeight + 0.2*HEIGHT));
        }
      };
      timer.start();
    }


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
