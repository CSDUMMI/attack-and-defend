/**
 * @author Joris Gutjahr
 * A single TilePane
*/
package IO;
import javafx.scene.layout.StackPane;
import javafx.scene.control.Label;
import javafx.scene.shape.Rectangle;
import javafx.scene.input.MouseEvent;
import javafx.event.EventHandler;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javafx.geometry.Insets;
import game.Tile;
import game.User;

public class TilePane {
  public static StackPane tilePane( Tile tile
                                  , double width
                                  , double height
                                  , User user
                                  , EventHandler<? super MouseEvent> onClick
                                  ) {
    Color color;

    if(user.attacks(tile)) {
      color = Color.RED;
    } else if(tile.owner == user && user.isSelected(tile)) {
      color = Color.BLUEVIOLET;
    } else if(tile.owner == user) {
      color = Color.FORESTGREEN;
    } else {
      color = Color.SLATEGRAY;
    }

    Label label = new Label(tile.owner.getName());
    Rectangle rect = new Rectangle(width, height, color);

    rect.setOnMousePressed(onClick);
    label.setOnMousePressed(onClick);

    StackPane pane = new StackPane();
    pane.getChildren().addAll(rect, label);
    pane.setPadding(new Insets(20));
    pane.relocate(tile.getX() * width, tile.getY() * height);
    return pane;
  }
}
