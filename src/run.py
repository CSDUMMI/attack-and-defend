#!/usr/bin/env python3
import sys
import os

if len(sys.argv) == 2 and sys.argv[1] == "build":
 	os.system(f"javac -d bin/ --module-path {os.environ['JAVAFX_PATH']} --add-modules javafx.controls,javafx.fxml Main.java")
else:
	os.system(f"java -classpath bin --module-path {os.environ['JAVAFX_PATH']} --add-modules javafx.controls,javafx.fxml Main")
